<#
.SYNOPSIS
    Deploy new Windows 10 system
.DESCRIPTION
    This script will properly debloat and configure a new Windows 10 installation.
.PARAMETER Drivespace
    Specifies the size of storage space to check
.PARAMETER Memory
    Specifies the size of memory space to check
.EXAMPLE
    Deploy-Windows-10.ps1
.EXAMPLE
    Deploy-Windows-10.ps1 -Drivespace <NUMBER > 40>
.EXAMPLE
    Deploy-Windows-10.ps1 -Memory <NUMBER > 4>
.NOTES
    Version:    1.1
    Author:     Jacob Stauffer
#>

param (
    [Parameter(Mandatory=$False, ValueFromPipeline=$True)]
    [ValidateRange(40,4096)]
    [int]$Drivespace = 40,
    [Parameter(Mandatory=$False, ValueFromPipeline=$True)]
    [ValidateRange(4,512)]
    [int]$Memory = 4
)

function InstallBoxstarter () {
    process {
        # Try to install BoxStarter as is first, then fall back to be over trusing only if this step fails.
        Write-Host "[+] Installing BoxStarter..." -ForegroundColor Green
        try {
            Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://boxstarter.org/bootstrapper.ps1')); get-boxstarter -Force
            return $true
        } catch {
            Write-Host "`t[!] Could not install boxstarter without trust, escalating privileges" -ForegroundColor Yellow
        }

        # https://stackoverflow.com/questions/11696944/powershell-v3-invoke-webrequest-https-error
        # Allows current PowerShell session to trust all certificates
        # Also a good find: https://www.briantist.com/errors/could-not-establish-trust-relationship-for-the-ssltls-secure-channel/
        try {
            Add-Type @"
            using System.Net;
            using System.Security.Cryptography.X509Certificates;
            public class TrustAllCertsPolicy : ICertificatePolicy {
            public bool CheckValidationResult(
                ServicePoint srvPoint, X509Certificate certificate,
                WebRequest request, int certificateProblem) {
                return true;
                }
            }
"@
        } catch {
            Write-Host "`t[!] Failed to add new type" -ForegroundColor Yellow
        }
        try {
            $AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
        } catch {
            Write-Host "`t[!] Failed to find SSL type...1" -ForegroundColor Yellow
        }
        try {
            $AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls'
        } catch {
            Write-Host "`t[!] Failed to find SSL type...2" -ForegroundColor Yellow
        }
        $prevSecProtocol = [System.Net.ServicePointManager]::SecurityProtocol
        $prevCertPolicy = [System.Net.ServicePointManager]::CertificatePolicy

        # Become overly trusting
        [System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
        [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
        # download and instal boxstarter
        Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://boxstarter.org/bootstrapper.ps1')); get-boxstarter -Force
        # Restore previous trust settings for this PowerShell session
        # Note: SSL certs trusted from installing BoxStarter above will be trusted for the remaining PS session
        [System.Net.ServicePointManager]::SecurityProtocol = $prevSecProtocol
        [System.Net.ServicePointManager]::CertificatePolicy = $prevCertPolicy
        return $true
    }
}

# Check to make sure script is run as administrator
Write-Host "[+] Checking if script is running as administrator..." -ForegroundColor Green
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal( [Security.Principal.WindowsIdentity]::GetCurrent() )
if (-Not $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Write-Host "[*] Please run this script as administrator`n" -ForegroundColor Red
    Read-Host  "Press any key to continue"
    exit
}

# Check to make sure host is supported
Write-Host "[+] Checking operating system compatibility..." -ForegroundColor Green
if (-Not (([System.Environment]::OSVersion.Version.Major -eq 10))) {
    Write-Host "`t[*] $((Get-WmiObject -class Win32_OperatingSystem).Caption) is not supported, please use Windows 7 Service Pack 1 or Windows 10" -ForegroundColor Red
    exit
} 
else {
    Write-Host "`t[!] $((Get-WmiObject -class Win32_OperatingSystem).Caption) supported"
}

# Check if system has enough memory
Write-Host "[+] Checking system memory..." -ForegroundColor Green
$localmemory = Get-WmiObject -class win32_ComputerSystem | Select-Object -Expand TotalPhysicalMemory
Start-Sleep -Seconds 1
if (-Not ([Math]::Round(($localmemory) / 1Gb) -ge $Memory)) {
    Write-Host "`t[*] Your system has less than $Memory GB of memory. Please increase your memory capacity and retry." -ForegroundColor Red
    exit
}

# Check and warn if diskspace is too low
Write-Host "[+] Checking for available diskspace..." -ForegroundColor Green
$disk = Get-PSDrive ${Env:SystemDrive}[0]
Start-Sleep -Seconds 1
if (-Not (($disk.free) / 1Gb -ge $Drivespace)) {
    Write-Host "`t[?] Warning: You have less than 40Gb of free diskspace left. Are you sure you want to continue? (Y/N) " -ForegroundColor Yellow -NoNewline
    $response = Read-Host
    if ($response -ne "Y") {
        Write-Host "`t[*] Exiting..." -ForegroundColor Red 
        exit
    }
    Write-Host "`t[!] Continuing ..."
}

# Create a windows restore point
Write-Host "[+] Creating a restore point..." -ForegroundColor Green
Enable-ComputerRestore -Drive "C:\"
Checkpoint-Computer -Description "RestorePoint1" -RestorePointType "MODIFY_SETTINGS"

# Get user credentials for autologin during reboots
# Write-Host "[+] Getting user credentials ..."
# Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds" -Name "ConsolePrompting" -Value $True
# if ([string]::IsNullOrEmpty($password)) {
#     $cred=Get-Credential $env:username
# } else {
#     $spasswd=ConvertTo-SecureString -String $password -AsPlainText -Force
#     $cred=New-Object -TypeName "System.Management.Automation.PSCredential" -ArgumentList $env:username, $spasswd
# }

# Install BoxStarter
$bs = InstallBoxstarter
if (-Not $bs) {
    Write-Host "`t[!] Could not install boxstarter" -ForegroundColor Red
    exit
}

$Boxstarter.SuppressLogging=$True

Write-Host "[+] Running debloating script..." -ForegroundColor Green
New-PackageFromScript .\debloat.ps1 debloat_script
Install-BoxStarterPackage -PackageName debloat_script -DisableReboots

Write-Host "[+] Running disabling (enable privacy) script..." -ForegroundColor Green
New-PackageFromScript .\disable.ps1 disable_script
Install-BoxStarterPackage -PackageName disable_script -DisableReboots

Write-Host "[+] Running UI tweaking script..." -ForegroundColor Green
New-PackageFromScript .\tweaks.ps1 tweaks_script
Install-BoxStarterPackage -PackageName tweaks_script -DisableReboots

Enable-MicrosoftUpdate
Install-WindowsUpdate -acceptEula