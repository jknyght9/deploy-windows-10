# Deploy Windows 10

This project is a set of PowerShell scripts that can be used to deploy Windows 10 to a remote machine. Its purpose is to remove bloatware and other privacy related configurations installed by default.

## Project Layout

The `scripts` directory contains several PowerShell scripts that were compiled for reference purposes. Several of the functions within these scripts make up the main scripts located in the root of this project.

The `Deploy-Windows10.ps1` script is the main script of the project and calls several scripts within the root directory.

## Before Getting Started

**READ THIS BEFORE STARTING!** This script will make several changes to your Windows 10 installation! It is **HIGHLY** recommended that you backup your system before running this script, or test this in a virtual machine. Under the current license, the developer is NOT responsible for any damage that may occur to your system or data that may be lost.

## Getting Started

1. Create a new Windows 10 system
2. Clone this repository
3. Run the `Deploy-Windows10.ps1` script.

    ```powershell
        powershell -ExecutionPolicy Unrestricted -File .\Deploy-Windows10.ps1
    ```

4. Get coffee and enjoy your new Windows 10 system!

## References

Giving credit where it is due, the following references were used for this project:

[https://christitus.com/debloat-windows-10-2020/](https://christitus.com/debloat-windows-10-2020/)

[https://github.com/mandiant/flare-vm](https://github.com/mandiant/flare-vm)

[https://github.com/Sycnex/Windows10Debloater](https://github.com/Sycnex/Windows10Debloater)

<a href="https://www.buymeacoffee.com/jstauffer" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>