# Set wallpaper
$wallpaper_url = "https://cc-public.s3.amazonaws.com/is4523.jpg"
$tool_dir = "IS4523"
Write-Host "[+] Setting wallpaper..." -ForegroundColor Green
New-Item -Path "C:\ProgramData" -Name $tool_dir -ItemType "directory"
Set-Location "C:\ProgramData\" + $tool_dir
Invoke-WebRequest $wallpaper_url -OutFile "background.jpg"
New-Item -Path HKLM:\Software\Policies\Microsoft\Windows -Name Personalization -Force
Set-ItemProperty -Path HKLM:\Software\Policies\Microsoft\Windows\Personalization -Name LockScreenImage -Value "C:\ProgramData\$tool_dir\background.jpg"
New-Item -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies -Name System -Force
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\System -Name Wallpaper -Value "C:\ProgramData\$tool_dir\background.jpg"
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\System -Name WallpaperStyle -Value "4"