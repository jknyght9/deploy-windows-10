# Remove all default desktop icons from Public folder
Write-Host "[+] Removing default desktop icons..." -ForegroundColor Green
Get-ChildItem -Path $env:PUBLIC\Desktop | ForEach-Object { Remove-Item -Path $_.FullName -Force }