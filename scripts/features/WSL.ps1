# For automated Linux distribution installation, see https://docs.microsoft.com/en-us/windows/wsl/install-on-server
# Install Linux Subsystem - Applicable since Win10 1607 and Server 1709
Write-Host "[+] Installing Linux Subsystem...\n" -ForegroundColor Green
Get-WindowsOptionalFeature -Online | Where-Object { $_.FeatureName -eq "Microsoft-Windows-Subsystem-Linux" } | Enable-WindowsOptionalFeature -Online -NoRestart -WarningAction SilentlyContinue | Out-Null

# Uninstall Linux Subsystem - Applicable since Win10 1607 and Server 1709
Write-Host "[+] Uninstalling Linux Subsystem...`n" -ForegroundColor Green
Get-WindowsOptionalFeature -Online | Where-Object { $_.FeatureName -eq "Microsoft-Windows-Subsystem-Linux" } | Disable-WindowsOptionalFeature -Online -NoRestart -WarningAction SilentlyContinue | Out-Null

# Install Linux kernel
Write-Host "[+] Installing Linux Kernel..." -ForegroundColor Green
Invoke-WebRequest -Uri "https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi" -OutFile $env:TEMP\wsl_update_x64.msi -UseBasicParsing
Start-Process -FilePath msiexec -ArgumentList "/i $env:TEMP\wsl_update_x64.msi /quiet /norestart" -WorkingDirectory $env:TEMP -Wait