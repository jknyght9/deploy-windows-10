# Install the Ubuntu WSL distribution
Write-Host "[+] Installing the Ubuntu WSL distribution..." -ForegroundColor Green
Set-Location "C:\Temp"
$ubu_url = "https://aka.ms/wslubuntu2004"
# $kali_url = "https://aka.ms/wsl-kali-linux-new"
$ProgressPreference = "SilentlyContinue"
Invoke-WebRequest -Uri $ubu_url -OutFile "ubuntu2004.appx" -UseBasicParsing
Add-AppxPackage "ubuntu2004.appx"
# $userenv = [System.Environment]::GetEnvironmentVariable("Path", "User")
# [System.Environment]::SetEnvironmentVariable("PATH", $userenv + ";C:\Users\Administrator\Ubuntu", "User")