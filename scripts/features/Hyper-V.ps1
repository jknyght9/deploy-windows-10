# Uninstall Hyper-V - Not applicable to Home
Write-Host "[+] Uninstalling Hyper-V..." -ForegroundColor Green
If ((Get-CimInstance -Class "Win32_OperatingSystem").ProductType -eq 1) {
	Get-WindowsOptionalFeature -Online | Where-Object { $_.FeatureName -eq "Microsoft-Hyper-V-All" } | Disable-WindowsOptionalFeature -Online -NoRestart -WarningAction SilentlyContinue | Out-Null
} Else {
	Uninstall-WindowsFeature -Name "Hyper-V" -IncludeManagementTools -WarningAction SilentlyContinue | Out-Null
}

# Install Hyper-V - Not applicable to Home
Write-Host "[+] Installing Hyper-V..." -ForegroundColor Green
If ((Get-CimInstance -Class "Win32_OperatingSystem").ProductType -eq 1) {
	Get-WindowsOptionalFeature -Online | Where-Object { $_.FeatureName -eq "Microsoft-Hyper-V-All" } | Enable-WindowsOptionalFeature -Online -NoRestart -WarningAction SilentlyContinue | Out-Null
} Else {
	Install-WindowsFeature -Name "Hyper-V" -IncludeManagementTools -WarningAction SilentlyContinue | Out-Null
}