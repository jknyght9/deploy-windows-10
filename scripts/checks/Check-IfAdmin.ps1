# Check to make sure script is run as administrator
Write-Host "[+] Checking if script is running as administrator..." -ForegroundColor Green
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal( [Security.Principal.WindowsIdentity]::GetCurrent() )
if (-Not $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Write-Host "[*] Please run this script as administrator" -ForegroundColor Red
    Read-Host  "Press any key to continue"
    exit
}