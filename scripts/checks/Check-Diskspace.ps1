# Check and warn if diskspace is too low
$required=40
Write-Host "[+] Checking for available diskspace..." -ForegroundColor Green
$disk = Get-PSDrive ${Env:SystemDrive}[0]
Start-Sleep -Seconds 1
if (-Not (($disk.free)/1Gb -gt $required)) {
    Write-Host "`t[WRN} Warning: You have less than 40Gb of free diskspace left. Are you sure you want to continue? (Y/N) " -ForegroundColor Yellow -NoNewline
    $response = Read-Host
    if ($response -ne "Y") {
        Write-Host "`t[*] Exiting..." -ForegroundColor Red 
        exit
    }
    Write-Host "`t[!] Continuing ..."
}